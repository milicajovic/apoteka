package com.example.demo.controller;

import java.io.InputStream;
import java.io.OutputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.example.demo.repository.KategorijaRepository;
import com.example.demo.repository.KorisnikRepository;
import com.example.demo.repository.KupovinaRepository;
import com.example.demo.repository.ProizvodRepository;

import model.MilicaJovicKategorija;
import model.MilicaJovicKupovina;
import model.MilicaJovicProizvod;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;

@Controller
@RequestMapping(value = "/admin")
public class AdminControler {
	
	@Autowired
	KategorijaRepository kr;
	
	@Autowired
	ProizvodRepository pr;
	
	@Autowired
	KorisnikRepository kkr;
	
	@Autowired
	KupovinaRepository kupr;
	
	@RequestMapping(value="/sveKategorije", method=RequestMethod.GET)
	public String sveKategorije(HttpServletRequest request) {
		List<MilicaJovicKategorija> kategorija = kr.findAll();
		request.getSession().setAttribute("kategorija", kategorija);
		return "sacuvajProizvod";
	}

	@RequestMapping(value="/saveProizvod", method=RequestMethod.POST)
	public String saveProizvod(String naziv, String opis, String cena, String akcija, Integer idk, Model m) {
	
		
		MilicaJovicProizvod p = new MilicaJovicProizvod();
		p.setNaziv(naziv);
		p.setCena(cena);
		p.setOpis(opis);
		p.setAkcija(akcija);
		MilicaJovicKategorija k = kr.findById(idk).get();
		p.setMilicaJovicKategorija(k);
		
		MilicaJovicProizvod proizvod = pr.save(p);
		m.addAttribute("p",proizvod);
		
		return "redirect:/admin/sveKategorije";
	}


	@RequestMapping(value = "/delete/{numIndex}", method = RequestMethod.GET)
	public String deleteProizvod(@PathVariable("numIndex") String naziv, Model m) {
		pr.deleteByNaziv(naziv);
		return "redirect:/auth/sviProizvodi";
	}
	
	
	
	@RequestMapping(value="/SviClanovi.pdf", method=RequestMethod.GET)
	public void showReport(HttpServletResponse response) throws Exception{
		
		
		response.setContentType("text/html");
		JRBeanCollectionDataSource dataSource = new JRBeanCollectionDataSource(kkr.findAll());
		InputStream inputStream = this.getClass().getResourceAsStream("/jasperreports/sviKorisnicii.jrxml");
		JasperReport jasperReport = JasperCompileManager.compileReport(inputStream);
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("naziv", "Benu");
		JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, params, dataSource);
		inputStream.close();

		response.setContentType("application/x-download");
		response.addHeader("Content-disposition", "attachment; filename=SviClanovi.pdf");
		OutputStream out = response.getOutputStream();
		JasperExportManager.exportReportToPdfStream(jasperPrint,out);
	}

	 @RequestMapping(value="/sveKupovine", method=RequestMethod.GET)
		public String sviProizvodi(HttpServletRequest request) {
			List<MilicaJovicKupovina> kupovina = kupr.findAll();
			request.getSession().setAttribute("kupovina", kupovina);
			return "kupovine";
		}

}
