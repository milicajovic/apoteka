package com.example.demo.controller;


import java.util.List;


import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.example.demo.repository.KategorijaRepository;
import com.example.demo.repository.KorisnikRepository;
import com.example.demo.repository.ProizvodRepository;
import com.example.demo.repository.UlogaRepository;

import model.MilicaJovicKategorija;
import model.MilicaJovicKorisnik;
import model.MilicaJovicProizvod;
import model.MilicaJovicUloga;




@Controller
@ControllerAdvice
@RequestMapping(value = "/auth")
public class LoginControler {
	@Autowired
	UlogaRepository ur;
	
	@Autowired
	KorisnikRepository kkr;
	
	@Autowired
	ProizvodRepository pr;
	
	@Autowired
	KategorijaRepository kr;
	
	
	@ModelAttribute
	public void getRoles(Model model) {
		List<MilicaJovicUloga> roles=ur.findAll();
		model.addAttribute("roles", roles);
		
	}
	
	   @RequestMapping(value = "/loginPage", method = RequestMethod.GET)
	    public String loginPage() {    
	    	return "login";
	     
	    }
	   
	   @RequestMapping(value="pocetna", method=RequestMethod.GET) 
		public String pocetna() { 
			return "Pocetna";
		}
   
	   
	    @RequestMapping(value = "registerUser", method = RequestMethod.GET)
		public String newUser(Model model) {
			MilicaJovicKorisnik u = new MilicaJovicKorisnik();
			
			model.addAttribute("user", u);
			return "register";
		}
	 
	   @RequestMapping(value = "register", method = RequestMethod.POST)
		public String saveUser(@ModelAttribute("user") MilicaJovicKorisnik u) {
	    	BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
	     	u.setSifra(passwordEncoder.encode(u.getSifra()));
			
			for (MilicaJovicUloga ur : u.getMilicaJovicUlogas()) {
				ur.addKorisnik(u);
				
			}
	    	kkr.save(u);
			return "login";

		}
	    
	    @RequestMapping(value="/logout", method = RequestMethod.GET)
	    public String logoutPage (HttpServletRequest request, HttpServletResponse response){
	        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
	        if (auth != null){    
	            SecurityContextHolder.getContext().setAuthentication(null);
	        }
	        return "redirect:/auth/loginPage";
	    }
	    
	  
	  
	    @RequestMapping(value="/sviProizvodi", method=RequestMethod.GET)
		public String sviProizvodi(HttpServletRequest request) {
			List<MilicaJovicProizvod> proizvod = pr.findAll();
			request.getSession().setAttribute("proizvod", proizvod);
			return "proizvodi";
		}
	    
	    
	    @RequestMapping(value="/sveKategorije", method=RequestMethod.GET)
		public String sveKategorije(HttpServletRequest request) {
			List<MilicaJovicKategorija> kategorija = kr.findAll();
			request.getSession().setAttribute("kategorija", kategorija);
			return "opis";
		}

	    
	   @RequestMapping(value="/opisProizvoda", method=RequestMethod.GET) 
		public String opisProizvoda(HttpServletRequest request, Integer idk) { 
			MilicaJovicKategorija k = kr.findById(idk).get();
			List<MilicaJovicProizvod> proizvodi = pr.findByMilicaJovicKategorija(k);
			request.getSession().setAttribute("proizvodi", proizvodi);
			return "opis";
		
	   }
	  
	   @RequestMapping(value = "/sveAkcije", method = RequestMethod.GET)
		public String sveAkcije(Model m) {
			m.addAttribute("proizvod", pr.sviAkcija());
			return "akcija";
		}
	   

	    
}
