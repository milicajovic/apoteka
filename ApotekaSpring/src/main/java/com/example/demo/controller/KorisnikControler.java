package com.example.demo.controller;

import java.io.InputStream;
import java.io.OutputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.example.demo.repository.KategorijaRepository;
import com.example.demo.repository.KupovinaRepository;
import com.example.demo.repository.ProizvodRepository;

import model.MilicaJovicKategorija;
import model.MilicaJovicKupovina;
import model.MilicaJovicProizvod;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;

@Controller
@RequestMapping(value = "/user")
public class KorisnikControler {


	@Autowired
	ProizvodRepository pr;
	
	@Autowired
	KategorijaRepository kr;
	
	@Autowired
	KupovinaRepository kkr;
	
	 @RequestMapping(value="/sveKategorije", method=RequestMethod.GET)
		public String sveKategorije(HttpServletRequest request) {
			List<MilicaJovicKategorija> kategorija = kr.findAll();
			request.getSession().setAttribute("kategorija", kategorija);
			return "opis";
		}

	    
	   @RequestMapping(value="/opisProizvoda", method=RequestMethod.GET) 
		public String opisProizvoda(HttpServletRequest request, Integer idk) { 
			MilicaJovicKategorija k = kr.findById(idk).get();
			List<MilicaJovicProizvod> proizvodi = pr.findByMilicaJovicKategorija(k);
			request.getSession().setAttribute("proizvodi", proizvodi);
			return "opis";
		
	   }
	   
	   @RequestMapping(value="/sviProizvodi", method=RequestMethod.GET)
	   public String sviProizvodi(HttpServletRequest request) {
		   List<MilicaJovicProizvod> proizvod=pr.findAll();
		   request.getSession().setAttribute("proizvod", proizvod);
		   return "kupovina";
	   }
	   
	   @RequestMapping(value="/kupovinaa", method=RequestMethod.POST)
	   public String sacuvajKupovinu(Integer kolicina, Integer idp, Model m) {
		   MilicaJovicKupovina k=new MilicaJovicKupovina();  
		   
		   MilicaJovicProizvod p=pr.findById(idp).get();
		   k.setMilicaJovicProizvod(p);
		   k.setKolicina(kolicina);
		   
		   MilicaJovicKupovina kupovina = kkr.save(k);
		   m.addAttribute("kupovina", kupovina);
		   return "kupovina";
		}
	
	   
	   @RequestMapping(value="/generisiIzvestaj", method=RequestMethod.GET) 
		public void generisiIzvestaj(HttpServletRequest request, HttpServletResponse response) throws Exception { 
			List<MilicaJovicProizvod> proizvod = (List<MilicaJovicProizvod>)request.getSession().getAttribute("proizvodi");
		
			response.setContentType("text/html");
			JRBeanCollectionDataSource dataSource = new JRBeanCollectionDataSource(proizvod);
			InputStream inputStream = this.getClass().getResourceAsStream("/jasperreports/proizvodiKategorije.jrxml");
			JasperReport jasperReport = JasperCompileManager.compileReport(inputStream);
			Map<String, Object> params = new HashMap<String, Object>();
			params.put("kategorija", proizvod.get(0).getMilicaJovicKategorija().getNazivKategorije());
			JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, params, dataSource);
			inputStream.close();
			
			
			response.setContentType("application/x-download");
			response.addHeader("Content-disposition", "attachment; filename=proizvodiKategorije.pdf");
			OutputStream out = response.getOutputStream();
			JasperExportManager.exportReportToPdfStream(jasperPrint,out);
		}
		
}
