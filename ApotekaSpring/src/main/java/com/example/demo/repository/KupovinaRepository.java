package com.example.demo.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import model.MilicaJovicKupovina;

public interface KupovinaRepository extends JpaRepository<MilicaJovicKupovina, Integer>{

}
