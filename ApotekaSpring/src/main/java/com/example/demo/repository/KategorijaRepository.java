package com.example.demo.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import model.MilicaJovicKategorija;

public interface KategorijaRepository extends JpaRepository<MilicaJovicKategorija, Integer>{
	
}
