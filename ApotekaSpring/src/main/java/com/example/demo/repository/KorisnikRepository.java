package com.example.demo.repository;



import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import model.MilicaJovicKorisnik;



@Repository
@Transactional
public interface KorisnikRepository extends JpaRepository<MilicaJovicKorisnik, Integer>{
	MilicaJovicKorisnik findByKorisnickoIme(String korisnickoIme);
	
	
}