package com.example.demo.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.data.jpa.repository.Query;

import model.MilicaJovicKategorija;
import model.MilicaJovicProizvod;
@Transactional
public interface ProizvodRepository extends JpaRepository<MilicaJovicProizvod, Integer>{
	List<MilicaJovicProizvod> findByMilicaJovicKategorija(MilicaJovicKategorija milicaJovicKategorija);
	
	@Query("select p from MilicaJovicProizvod p where akcija is not null")
	public List<MilicaJovicProizvod> sviAkcija();
	
	public void deleteByNaziv(String naziv);
	
	}
