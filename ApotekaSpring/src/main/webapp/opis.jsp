<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri='http://java.sun.com/jsp/jstl/core' prefix='c'%>
<%@ taglib prefix="security" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<!DOCTYPE html>
<html>
<head>
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/style/style.css">
<meta charset="ISO-8859-1">
<title>Opis proizvoda</title>
</head>
<body>

<ul>
	<li><a href="/Apoteka/Pocetna.jsp">Početna</a></li>
	<li><a href="/Apoteka/auth/loginPage">Prijavite se</a></li>
	<li><a href="/Apoteka/Onama.jsp">O nama</a></li>	
	<li><a href="/Apoteka/Kontakt.jsp">Kontakt</a></li>
	<li><a href="/Apoteka/auth/sviProizvodi">Naši proizvodi</a></li>
	<li><a href="/Apoteka/auth/sveAkcije">Akcijske cene</a></li>
	<li><a href="/Apoteka/Zanimljivo.jsp">Zanimljivi članci</a></li>
	
	<security:authorize access="isAuthenticated()">
	<sec:authorize access="hasRole('admin')">
	<li><a href="/Apoteka/admin/sveKategorije">Novi proizvod</a>
	</sec:authorize>
	<li><a href="/Apoteka/auth/logout">Odjava</a></li>
	</security:authorize>
</ul>
<div class="main">
	<img src="${pageContext.request.contextPath}/img/apoteka2.png" class="center"/>
</div>
<div class="center4" style="font-size:20px; text-align:center;"> 
<form action="/Apoteka/auth/opisProizvoda" method="get">

		<select name="idk">
			<c:forEach items="${kategorija}" var="k">
				<option value="${k.idk} "> ${k.nazivKategorije}</option>
			</c:forEach>
		</select>
		<br/><br/>
		<input type="submit" value="Prikaz"><br/>
</form>
<c:if test="${! empty proizvodi }">

<form action="/Apoteka/user/generisiIzvestaj" method="get">

		<table border=1>
			<tr><th>Naziv</th><th>Opis</th></tr>
			
			<c:forEach items="${proizvodi }" var="p">
				
				<tr>
					<td>${p.naziv}</td>
					<td>${p.opis }</td>		
				</tr>
			</c:forEach>
		</table>
		<br/>
		<input type="submit" value="Detaljniji pogled kategorije">
		
</form>
</c:if>
</div>
		

</body>
</html>