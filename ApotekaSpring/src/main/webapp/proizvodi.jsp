<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri='http://java.sun.com/jsp/jstl/core' prefix='c'%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
 <%@ taglib prefix="security" uri="http://www.springframework.org/security/tags" %>
<!DOCTYPE html>
<html>
<head>
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/style/style.css">
<meta charset="ISO-8859-1">
<title>Proizvodi</title>
</head>
<body>

<ul>
	<li><a href="/Apoteka/Pocetna.jsp">Početna</a></li>
	<li><a href="/Apoteka/auth/loginPage">Prijavite se</a></li>
	<li><a href="/Apoteka/Onama.jsp">O nama</a></li>	
	<li><a href="/Apoteka/Kontakt.jsp">Kontakt</a></li>
	<li><a href="/Apoteka/auth/sviProizvodi">Naši proizvodi</a></li>
	<li><a href="/Apoteka/auth/sveAkcije">Akcijske cene</a></li>
	<li><a href="/Apoteka/Zanimljivo.jsp">Zanimljivi članci</a></li>
	
	<security:authorize access="isAuthenticated()">
	<sec:authorize access="hasRole('admin')">
	<li><a href="/Apoteka/admin/sveKategorije">Novi proizvod</a>
	</sec:authorize>
	<li><a href="/Apoteka/auth/logout">Odjava</a></li>
	</security:authorize>
</ul>
<div class="main">
	<img src="${pageContext.request.contextPath}/img/apoteka2.png" class="center"/>
</div>

<div class="center4" style="font-size:20px; text-align:center;"> 
		<table >
			<tr><th>Kategorija</th><th>Naziv</th><th>Cena</th></tr>
			
			<c:forEach items="${proizvod }" var="p">
				
				<tr>
					<td>${p.milicaJovicKategorija.nazivKategorije}</td>
					<td>${p.naziv }</td>
					<td>${p.cena }</td>
					<sec:authorize access="hasRole('admin')">
  					 <td><a href="<c:url value='/admin/delete/${p.naziv}'/>"> Uklonite</a></td>
    				</sec:authorize>
					
				</tr>
			</c:forEach>
		</table>
		<br/>
		<a href="/Apoteka/auth/sveKategorije">Pogledajte opise proizvoda</a><br/>
		<sec:authorize access="hasRole('admin')">
		<a href="/Apoteka/admin/sveKupovine">Kupljeni proizvodi</a>
		</sec:authorize>
</div>
</body>
</html>