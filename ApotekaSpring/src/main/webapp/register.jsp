<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri='http://java.sun.com/jsp/jstl/core' prefix='c'%>
<%@ taglib prefix="sf" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="security" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<!DOCTYPE html>
<html>
<head>
<style type="text/css">
.main {
  margin-left: 500px; 
  padding: 0px 10px;
  margin-top: 140px;
}



.header {
	padding: 15px;
	text-align: center;
	background: white;
	color: white;
	font-size: 30px;
}

ul {
	list-style-type: none;
	margin: 0;
	padding: 0;
	overflow: hidden;
	background-color:#DEF2F1;
}

li {
	float: right;
}

li a {
	display:inline-block; 
	color: #06159B;
	text-align: center;
	padding: 20px 16px;
	text-decoration: none;
	font-size: 25px;
}

a:hover:not(.active) {
	background-color: #ECFDFF;
}


</style>
<meta charset="ISO-8859-1">
<title>Registracija</title>
</head>
<body>

<ul>
	<li><a href="/Apoteka/Pocetna.jsp">Početna</a></li>
	<li><a href="/Apoteka/auth/loginPage">Prijavite se</a></li>
	<li><a href="/Apoteka/Onama.jsp">O nama</a></li>	
	<li><a href="/Apoteka/Kontakt.jsp">Kontakt</a></li>
	<li><a href="/Apoteka/auth/sviProizvodi">Naši proizvodi</a></li>
	<li><a href="/Apoteka/auth/sveAkcije">Akcijske cene</a></li>
	<li><a href="/Apoteka/Zanimljivo.jsp">Zanimljivi članci</a></li>
	
	<security:authorize access="isAuthenticated()">
	<sec:authorize access="hasRole('admin')">
	<li><a href="/Apoteka/admin/sveKategorije">Novi proizvod</a>
	</sec:authorize>
	<li><a href="/Apoteka/auth/logout">Odjava</a></li>
	</security:authorize>
</ul>

<div class="main">
	


<sf:form modelAttribute="user"  action="register"  method="post">
  <table>
  	<tr>
  	   <td>Ime:</td><td><sf:input  path="ime"/>
  	   </td>
  	</tr>
  	<tr>
  	   <td>Prezime:</td><td><sf:input  path="prezime"/>
  	   </td>
  	</tr>
  		<tr>
  	   <td>Korisnicko ime:</td><td><sf:input  path="korisnickoIme"/>
  	 </td>
  	</tr>
  	<tr>
  	   <td>Sifra:</td><td><sf:password  path="sifra"/></td>
  	</tr>
  	<tr>
  	  <td>
  	   Uloga
  	  </td>
  	  <td>
  	  <sf:select path="milicaJovicUlogas" items="${roles}" itemValue="idUloga" itemLabel="naziv"/>
	   </td>
	</tr>
  	<tr><td/><td><input type="submit" value="Sacuvaj"></tr>
  	</table>
</sf:form>
</div>
</body>
</html>