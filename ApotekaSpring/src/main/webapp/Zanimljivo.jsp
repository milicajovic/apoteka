<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri='http://java.sun.com/jsp/jstl/core' prefix='c'%>
<%@ taglib prefix="security" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<!DOCTYPE html>
<html>
<head>
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/style/style.css">
<meta charset="ISO-8859-1">
<title>Zanimljivosti</title>
</head>
<body>

<ul>
	<li><a href="/Apoteka/Pocetna.jsp">Početna</a></li>
	<li><a href="/Apoteka/auth/loginPage">Prijavite se</a></li>
	<li><a href="/Apoteka/Onama.jsp">O nama</a></li>	
	<li><a href="/Apoteka/Kontakt.jsp">Kontakt</a></li>
	<li><a href="/Apoteka/auth/sviProizvodi">Naši proizvodi</a></li>
	<li><a href="/Apoteka/auth/sveAkcije">Akcijske cene</a></li>
	<li><a href="/Apoteka/Zanimljivo.jsp">Zanimljivi članci</a></li>
	
	<security:authorize access="isAuthenticated()">
	<sec:authorize access="hasRole('admin')">
	<li><a href="/Apoteka/admin/sveKategorije">Novi proizvod</a>
	</sec:authorize>
	<li><a href="/Apoteka/auth/logout">Odjava</a></li>
	</security:authorize>
</ul>
<br/><br/><br/>


<table border="7" bordercolor="#DEF2F1">
	<tr>
		<td>
		<div class="main" style="font-size:30px">
			<b>Kako se rešiti alergije?</b><br/><br/>
		
				<img src="${pageContext.request.contextPath}/img/farmaceut.png" class="center"/>
		</div>
		<div class="main2">
				1. Ulje crnog kima (Nigella Sativa seed oil) − koristilo se još u vreme faraona, a naučna istraživanja ove biljke sprovode se intenzivno u poslednjih dvadesetak godina. Danas postoji preko 200 studija koje su potvrdile da ulje crnog kima, ili crnog kumina kako se još naziva, sadrži veliki broj aktivnih sastojaka čiji je efekat na ljudski organizam veoma povoljan.<br/>
				2. Vitamin C i kalcijum − u slučaju alergije naše telo luči histamin koji je odgovoran za simptome alergije. Vitamin C smanjuje lučenje histamina, ubrzava njegovu razgradnju i pomaže njegovo uklanjanje iz tela.<br/> 
				3. Kvercetin je bioflavonoid snažnog antioksidativnog dejstva koji smanjuje nivo histamina tako što stabilizuje membrane mastocita i bazofila i tako sprečava izbacivanje histamina i serotonina u krv i tkiva. Naročito je efikasan zajedno sa bromelinom i vitaminom C čime pokazuje još jače protivupalno dejstvo.<br/> 
				4. Probiotici − najnovija istraživanja pokazuju povezanost između alergija i zdrave crevne flore. Probiotici uspostavljaju zdravu crevnu floru i pomažu kod alergija. Dokazano je da ljudi koji u ishranu uključe fermentisano mleko (jogurt, kefir, kiselo mleko) imaju bolju otpornost prema polenu i stvaraju više IgG antitela koja ih štite od alergijskih reakcija.
			</div>
		</td>	
	</tr>
</table>
<br/><br/>




<table border="7" bordercolor="#DEF2F1">
	<tr>
		<td>
		<div class="main" style="font-size:30px">
			<b>Hrana i visoke temperature</b><br/><br/>
		</div>
				<img src="${pageContext.request.contextPath}/img/hrana.png" class="center" />
		</div>
		<div class="main2">
		
				U letnjim mesecima češće je i trovanje hranom. Visoka temperatura pogoduje razvoju štetnih mikroorganizama (salmonela, stafilokoke, norovirusi…) u mnogim namirnicama koji mogu dovesti do različitih zdravstvenih tegoba.<br/>
				<b>Za doručak</b> treba uzimati hranu koja sadrži proteine i manje količine masti (npr. omleti od povrća, jaja, sira), uz neki integralni hleb, štangle, pecivo i sl. Takođe, mogu se konzumirati kaše od heljde, prosa, ovsenih pahuljica i dr. (ukoliko ne postoji intolerancija na gluten) sa dodatkom jogurta, nezaslađenog čaja, soka od pomorandže, kisele pavlake (jedna supena kašika). Ukoliko žitarice ili pahuljice nisu instant proizvodi, treba ih kratko prokuvati u malo vode (da puste sluz), a zatim dodati namirnice po izboru. Mogu se dodati jezgrasti plodovi (npr. orah, badem…) ili njihov puter.<br/>
				<b>Za letnji ručak</b> treba pripremati:<br/>

				• čorbe od mesa ili ribu uz salate (kao jedan obrok)<br/>

				• kuvano (ili grilovano) meso ili riba sa prilogom i salatom<br/>

				• salatu s termički obrađenim mesom ili ribom; sir ili pečurke<br/>
				<b>Za letnju večeru</b> treba uneti proteinski obrok, kao na primer: dinstane ili grilovane pečurke sa salatom, omlet od belanaca sa povrćem, sir, meso ili riba sa salatom.
			
			</div>
		</td>	
	</tr>
</table>
<br/><br/>

<table border="7" bordercolor="#DEF2F1">
	<tr>
		<td>
		<div class="main" style="font-size:30px">
			<b>Nega lica u 4 koraka</b><br/><br/>
		
				<img src="${pageContext.request.contextPath}/img/nega.png" class="center"/>
		</div>
		<div class="main2">
				  <b>Korak 1. UMIVANJE</b><br/>
				  Clear face Antibakterijska pena pH 5.5 nanosi se na suvu ili vlažnu kožu, ostavi da deluje 5 minuta i ispere mlakom vodom. Pogodna je za veće površine tela (grudi, leđa, vrat i kosmati delovi). Deluje na P. acnes i suzbija upaljene bubuljice, a pri tom ne isušuje kožu i nema perutanja.<br/>
				  <b>Korak 2. DUBINSKO ČIŠĆENJE</b><br/>
				  Clear face Tonik pH 5.5 nanosi se na komadić vate i njime se prebriše lice. Ne ispira se. Dodatno čisti već oprano lice i izvlači masnoću iz pora, pa je efikasan u otklanjanju crnih mitisera. Sastojak Hamamelis skuplja proširene pore i sprečava naknadno nakupljanje masnoće.<br/>
				  <b>Korak 3. NEGA</b><br/>
				  Clear face Gel za negu Ph 5.5 se koristi za negu lica umesto kreme. Ne sadrži masne komponente već samo hidirira i regeneriše kožu. Aloe Vera i alantoin umiruju crvenilo i iritaciju, a hijaluronska kiselina hidrira kožu. Pogodan je za negu masne kože bez obzira na uzrast.<br/>
				  <b>Korak 4. CILJANI TRETMAN</b><br/>
				  Clear face Tonirani krem i gel pH 5.5 za ciljano delovanje na bubuljicu. Antiseptično dejstvo u kombinaciji sa korektorom tena za prikrivanje nedostataka i ciljano delovanje.
			</div>
		</td>	
	</tr>
</table>
</body>
</html>