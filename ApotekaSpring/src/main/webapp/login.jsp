﻿<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="sf" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="security" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<!DOCTYPE html>
<html>
<head>
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/style/style.css">
<meta charset="UTF-8">
<title>Prijavite se</title>
</head>
<body>

<ul>
	<li><a href="/Apoteka/Pocetna.jsp">Početna</a></li>
	<li><a href="/Apoteka/auth/loginPage">Prijavite se</a></li>
	<li><a href="/Apoteka/Onama.jsp">O nama</a></li>	
	<li><a href="/Apoteka/Kontakt.jsp">Kontakt</a></li>
	<li><a href="/Apoteka/auth/sviProizvodi">Naši proizvodi</a></li>
	<li><a href="/Apoteka/auth/sveAkcije">Akcijske cene</a></li>
	<li><a href="/Apoteka/Zanimljivo.jsp">Zanimljivi članci</a></li>
	
	<security:authorize access="isAuthenticated()">
	<sec:authorize access="hasRole('admin')">
	<li><a href="/Apoteka/admin/sveKategorije">Novi proizvod</a>
	</sec:authorize>
	<li><a href="/Apoteka/auth/logout">Odjava</a></li>
	</security:authorize>
</ul>
<div class="main">
	<img src="${pageContext.request.contextPath}/img/onama.gif" class="center"/>
</div>

<div class="center3" style="font-size:20px; text-align:center;"><br/>
	<c:url var="loginUrl" value="/login" />
	<c:if test="${not empty param.error}">
		<div class="alert alert-danger">
			<p>Pogresni podaci.</p>
		</div>
	</c:if>
	<form action="${loginUrl}" method="post">		
	Korisnicko ime:<br/>
	<input type="text" name="username"><br/><br/>
			
	Sifra:<br/>
	<input type="password" name="password"><br/><br/>
			
	<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />		
	<input type="submit" value="Prijava"><br/><br/>
	
	Nemate nalog? <a href="/Apoteka/auth/registerUser">Registrujte se</a>
	</form>
	<br/>
	<sec:authorize access="hasRole('admin')">
	<a href="/Apoteka/admin/SviClanovi.pdf">Spisak registrovanih korisnika</a>
	</sec:authorize>
</div>

</body>
</html>