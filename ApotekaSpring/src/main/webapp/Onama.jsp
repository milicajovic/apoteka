<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
 <%@ taglib prefix="security" uri="http://www.springframework.org/security/tags" %>
 <%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<!DOCTYPE html>
<html>
<head>
 <link rel="stylesheet" type="text/css" href="style/style.css">
<meta charset="ISO-8859-1">
<title>O nama</title>
</head>
<body>
<ul>
	<li><a href="/Apoteka/Pocetna.jsp">Početna</a></li>
	<li><a href="/Apoteka/auth/loginPage">Prijavite se</a></li>
	<li><a href="/Apoteka/Onama.jsp">O nama</a></li>	
	<li><a href="/Apoteka/Kontakt.jsp">Kontakt</a></li>
	<li><a href="/Apoteka/auth/sviProizvodi">Naši proizvodi</a></li>
	<li><a href="/Apoteka/auth/sveAkcije">Akcijske cene</a></li>
	<li><a href="/Apoteka/Zanimljivo.jsp">Zanimljivi članci</a></li>
	
	<security:authorize access="isAuthenticated()">
	<sec:authorize access="hasRole('admin')">
	<li><a href="/Apoteka/admin/sveKategorije">Novi proizvod</a>
	</sec:authorize>
	<li><a href="/Apoteka/auth/logout">Odjava</a></li>
	</security:authorize>
</ul>

<div class="main">
	<img src="${pageContext.request.contextPath}/img/onama.gif" class="center"/>
</div>

<div class="center3" style="font-size:20px; text-align:center;">
	<p>
		Našu apoteku odlikuje nov i moderan pristup u sprovođenju farmaceutske usluge u oblasti medicinske zaštite i prevencije. Prilagođena zahtevima savremenog potrošača, apoteka pruža mogućnost samostalnog istraživanja proizvoda uz konsultacije sa stručnim farmaceutima i u skladu sa principima dobre farmaceutske prakse.
	</p>
 
	<p>
		Asortiman naših proizvoda odlikuje sveobuhvatnost i prilagodljivost zahtevima tržišta, potrebama klijenata i sezonalnosti. Asortiman obuhvata veliki broj različitih kategorija organizovanih prema principima preglednosti i dostupnosti. Otvoreni prostor naših apoteka svakom klijentu omogućava više informacija i saznanja u domenu kozmetike i drugih preparata za negu, dijetetskih suplemenata i drugog. Izbor je na našim klijentima, a naše je da omogućimo da u pravom okruženju izaberu pravi proizvod, u pravo vreme.
	</p>
</div>
</body>
</html>