package model;

import java.io.Serializable;
import javax.persistence.*;

import java.util.HashSet;
import java.util.List;
import java.util.Set;


/**
 * The persistent class for the MilicaJovicUloga database table.
 * 
 */
@Entity
@Table(name="MilicaJovicUloga")
@NamedQuery(name="MilicaJovicUloga.findAll", query="SELECT m FROM MilicaJovicUloga m")
public class MilicaJovicUloga implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="idUloga")
	private int idUloga;

	@Column(name="naziv")
	private String naziv;

	//bi-directional many-to-many association to MilicaJovicKorisnik
	@ManyToMany(fetch=FetchType.EAGER,mappedBy="milicaJovicUlogas")
	private Set<MilicaJovicKorisnik> milicaJovicKorisniks=new HashSet<>();

	public MilicaJovicUloga() {
	}

	public int getIdUloga() {
		return this.idUloga;
	}

	public void setIdUloga(int idUloga) {
		this.idUloga = idUloga;
	}

	public String getNaziv() {
		return this.naziv;
	}

	public void setNaziv(String naziv) {
		this.naziv = naziv;
	}

	public Set<MilicaJovicKorisnik> getMilicaJovicKorisniks() {
		return this.milicaJovicKorisniks;
	}

	public void setMilicaJovicKorisniks(Set<MilicaJovicKorisnik> milicaJovicKorisniks) {
		this.milicaJovicKorisniks = milicaJovicKorisniks;
	}
	
	public void addKorisnik(MilicaJovicKorisnik k) {
		this.milicaJovicKorisniks.add(k);
	}

}