package model;

import java.io.Serializable;
import javax.persistence.*;

import java.util.HashSet;
import java.util.List;
import java.util.Set;


/**
 * The persistent class for the MilicaJovicKorisnik database table.
 * 
 */
@Entity
@Table(name="MilicaJovicKorisnik")
@NamedQuery(name="MilicaJovicKorisnik.findAll", query="SELECT m FROM MilicaJovicKorisnik m")
public class MilicaJovicKorisnik implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="idKorisnik")
	private int idKorisnik;

	@Column(name="ime")
	private String ime;

	@Column(name="korisnickoIme")
	private String korisnickoIme;

	@Column(name="prezime")
	private String prezime;

	@Column(name="sifra")
	private String sifra;

	//bi-directional many-to-one association to MilicaJovicKupovina
	@OneToMany(mappedBy="milicaJovicKorisnik")
	private List<MilicaJovicKupovina> milicaJovicKupovinas;

	//bi-directional many-to-many association to MilicaJovicUloga
	@ManyToMany(fetch=FetchType.EAGER)
	@JoinTable(
		name="MilicaJovicUloga_has_MilicaJovicKorisnik"
		, joinColumns={
			@JoinColumn(name="MilicaJovicKorisnik_idKorisnik")
			}
		, inverseJoinColumns={
			@JoinColumn(name="MilicaJovicUloga_idUloga")
			}
		)
	private Set<MilicaJovicUloga> milicaJovicUlogas = new HashSet<>();

	public MilicaJovicKorisnik() {
	}

	public int getIdKorisnik() {
		return this.idKorisnik;
	}

	public void setIdKorisnik(int idKorisnik) {
		this.idKorisnik = idKorisnik;
	}

	public String getIme() {
		return this.ime;
	}

	public void setIme(String ime) {
		this.ime = ime;
	}

	public String getKorisnickoIme() {
		return this.korisnickoIme;
	}

	public void setKorisnickoIme(String korisnickoIme) {
		this.korisnickoIme = korisnickoIme;
	}

	public String getPrezime() {
		return this.prezime;
	}

	public void setPrezime(String prezime) {
		this.prezime = prezime;
	}

	public String getSifra() {
		return this.sifra;
	}

	public void setSifra(String sifra) {
		this.sifra = sifra;
	}

	public List<MilicaJovicKupovina> getMilicaJovicKupovinas() {
		return this.milicaJovicKupovinas;
	}

	public void setMilicaJovicKupovinas(List<MilicaJovicKupovina> milicaJovicKupovinas) {
		this.milicaJovicKupovinas = milicaJovicKupovinas;
	}

	public MilicaJovicKupovina addMilicaJovicKupovina(MilicaJovicKupovina milicaJovicKupovina) {
		getMilicaJovicKupovinas().add(milicaJovicKupovina);
		milicaJovicKupovina.setMilicaJovicKorisnik(this);

		return milicaJovicKupovina;
	}

	public MilicaJovicKupovina removeMilicaJovicKupovina(MilicaJovicKupovina milicaJovicKupovina) {
		getMilicaJovicKupovinas().remove(milicaJovicKupovina);
		milicaJovicKupovina.setMilicaJovicKorisnik(null);

		return milicaJovicKupovina;
	}

	public Set<MilicaJovicUloga> getMilicaJovicUlogas() {
		return this.milicaJovicUlogas;
	}

	public void setMilicaJovicUlogas(Set<MilicaJovicUloga> milicaJovicUlogas) {
		this.milicaJovicUlogas = milicaJovicUlogas;
	}
	
	public void addRole(MilicaJovicUloga r) {
		this.milicaJovicUlogas.add(r);
	}

}