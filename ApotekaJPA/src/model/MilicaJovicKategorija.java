package model;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the MilicaJovicKategorija database table.
 * 
 */
@Entity
@Table(name="MilicaJovicKategorija")
@NamedQuery(name="MilicaJovicKategorija.findAll", query="SELECT m FROM MilicaJovicKategorija m")
public class MilicaJovicKategorija implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="IDK")
	private int idk;
	
	@Column(name="nazivKategorije")
	private String nazivKategorije;

	//bi-directional many-to-one association to MilicaJovicProizvod
	@OneToMany(mappedBy="milicaJovicKategorija")
	private List<MilicaJovicProizvod> milicaJovicProizvods;

	public MilicaJovicKategorija() {
	}

	public int getIdk() {
		return this.idk;
	}

	public void setIdk(int idk) {
		this.idk = idk;
	}

	public String getNazivKategorije() {
		return this.nazivKategorije;
	}

	public void setNazivKategorije(String nazivKategorije) {
		this.nazivKategorije = nazivKategorije;
	}

	public List<MilicaJovicProizvod> getMilicaJovicProizvods() {
		return this.milicaJovicProizvods;
	}

	public void setMilicaJovicProizvods(List<MilicaJovicProizvod> milicaJovicProizvods) {
		this.milicaJovicProizvods = milicaJovicProizvods;
	}

	public MilicaJovicProizvod addMilicaJovicProizvod(MilicaJovicProizvod milicaJovicProizvod) {
		getMilicaJovicProizvods().add(milicaJovicProizvod);
		milicaJovicProizvod.setMilicaJovicKategorija(this);

		return milicaJovicProizvod;
	}

	public MilicaJovicProizvod removeMilicaJovicProizvod(MilicaJovicProizvod milicaJovicProizvod) {
		getMilicaJovicProizvods().remove(milicaJovicProizvod);
		milicaJovicProizvod.setMilicaJovicKategorija(null);

		return milicaJovicProizvod;
	}

}