package model;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the MilicaJovicProizvod database table.
 * 
 */
@Entity
@Table(name="MilicaJovicProizvod")
@NamedQuery(name="MilicaJovicProizvod.findAll", query="SELECT m FROM MilicaJovicProizvod m")
public class MilicaJovicProizvod implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="IDP")
	private int idp;

	@Column(name="akcija")
	private String akcija;

	@Column(name="cena")
	private String cena;

	@Column(name="naziv")
	private String naziv;

	@Lob
	@Column(name="opis")
	private String opis;

	@Lob
	@Column(name="slika")
	private byte[] slika;

	//bi-directional many-to-one association to MilicaJovicKupovina
	@OneToMany(mappedBy="milicaJovicProizvod")
	private List<MilicaJovicKupovina> milicaJovicKupovinas;

	//bi-directional many-to-one association to MilicaJovicKategorija
	@ManyToOne
	private MilicaJovicKategorija milicaJovicKategorija;

	public MilicaJovicProizvod() {
	}

	public int getIdp() {
		return this.idp;
	}

	public void setIdp(int idp) {
		this.idp = idp;
	}

	public String getAkcija() {
		return this.akcija;
	}

	public void setAkcija(String akcija) {
		this.akcija = akcija;
	}

	public String getCena() {
		return this.cena;
	}

	public void setCena(String cena) {
		this.cena = cena;
	}

	public String getNaziv() {
		return this.naziv;
	}

	public void setNaziv(String naziv) {
		this.naziv = naziv;
	}

	public String getOpis() {
		return this.opis;
	}

	public void setOpis(String opis) {
		this.opis = opis;
	}

	public byte[] getSlika() {
		return this.slika;
	}

	public void setSlika(byte[] slika) {
		this.slika = slika;
	}

	public List<MilicaJovicKupovina> getMilicaJovicKupovinas() {
		return this.milicaJovicKupovinas;
	}

	public void setMilicaJovicKupovinas(List<MilicaJovicKupovina> milicaJovicKupovinas) {
		this.milicaJovicKupovinas = milicaJovicKupovinas;
	}

	public MilicaJovicKupovina addMilicaJovicKupovina(MilicaJovicKupovina milicaJovicKupovina) {
		getMilicaJovicKupovinas().add(milicaJovicKupovina);
		milicaJovicKupovina.setMilicaJovicProizvod(this);

		return milicaJovicKupovina;
	}

	public MilicaJovicKupovina removeMilicaJovicKupovina(MilicaJovicKupovina milicaJovicKupovina) {
		getMilicaJovicKupovinas().remove(milicaJovicKupovina);
		milicaJovicKupovina.setMilicaJovicProizvod(null);

		return milicaJovicKupovina;
	}

	public MilicaJovicKategorija getMilicaJovicKategorija() {
		return this.milicaJovicKategorija;
	}

	public void setMilicaJovicKategorija(MilicaJovicKategorija milicaJovicKategorija) {
		this.milicaJovicKategorija = milicaJovicKategorija;
	}

}