package model;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;


/**
 * The persistent class for the MilicaJovicKupovina database table.
 * 
 */
@Entity
@Table(name="MilicaJovicKupovina")
@NamedQuery(name="MilicaJovicKupovina.findAll", query="SELECT m FROM MilicaJovicKupovina m")
public class MilicaJovicKupovina implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="idKupovina")
	private int idKupovina;

	@Temporal(TemporalType.DATE)
	@Column(name="datum")
	private Date datum;

	@Column(name="kolicina")
	private int kolicina;

	//bi-directional many-to-one association to MilicaJovicKorisnik
	@ManyToOne
	private MilicaJovicKorisnik milicaJovicKorisnik;

	//bi-directional many-to-one association to MilicaJovicProizvod
	@ManyToOne
	private MilicaJovicProizvod milicaJovicProizvod;

	public MilicaJovicKupovina() {
	}

	public int getIdKupovina() {
		return this.idKupovina;
	}

	public void setIdKupovina(int idKupovina) {
		this.idKupovina = idKupovina;
	}

	public Date getDatum() {
		return this.datum;
	}

	public void setDatum(Date datum) {
		this.datum = datum;
	}

	public int getKolicina() {
		return this.kolicina;
	}

	public void setKolicina(int kolicina) {
		this.kolicina = kolicina;
	}

	public MilicaJovicKorisnik getMilicaJovicKorisnik() {
		return this.milicaJovicKorisnik;
	}

	public void setMilicaJovicKorisnik(MilicaJovicKorisnik milicaJovicKorisnik) {
		this.milicaJovicKorisnik = milicaJovicKorisnik;
	}

	public MilicaJovicProizvod getMilicaJovicProizvod() {
		return this.milicaJovicProizvod;
	}

	public void setMilicaJovicProizvod(MilicaJovicProizvod milicaJovicProizvod) {
		this.milicaJovicProizvod = milicaJovicProizvod;
	}

}